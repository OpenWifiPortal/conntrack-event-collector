module gitlab.com/OpenWifiPortal/conntrack-event-collector

go 1.17

require (
	github.com/sirupsen/logrus v1.0.5
	github.com/spf13/cobra v0.0.2
	github.com/spf13/viper v1.0.2
	github.com/streadway/amqp v0.0.0-20180315184602-8e4aba63da9f
	gitlab.com/OpenWifiPortal/go-libs v0.0.0-20180413043940-b1a6b5867aca
)

require (
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/golang/snappy v0.0.0-20170215233205-553a64147049 // indirect
	github.com/hashicorp/errwrap v0.0.0-20141028054710-7554cd9344ce // indirect
	github.com/hashicorp/go-cleanhttp v0.0.0-20171218145408-d5fe4b57a186 // indirect
	github.com/hashicorp/go-multierror v0.0.0-20171204182908-b7773ae21874 // indirect
	github.com/hashicorp/go-rootcerts v0.0.0-20160503143440-6bb64b370b90 // indirect
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/hashicorp/vault v0.10.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/magiconair/properties v1.7.6 // indirect
	github.com/mitchellh/go-homedir v0.0.0-20161203194507-b8bc1bf76747 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/pelletier/go-toml v1.1.0 // indirect
	github.com/ryanuber/go-glob v0.0.0-20160226084822-572520ed46db // indirect
	github.com/sethgrid/pester v0.0.0-20180227223404-ed9870dad317 // indirect
	github.com/spf13/afero v1.1.0 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/jwalterweatherman v0.0.0-20180109140146-7c0cea34c8ec // indirect
	github.com/spf13/pflag v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20180411161317-d6449816ce06 // indirect
	golang.org/x/net v0.0.0-20180406214816-61147c48b25b // indirect
	golang.org/x/sys v0.0.0-20180406135729-3b87a42e500a // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
