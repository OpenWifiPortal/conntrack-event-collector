#!/bin/bash

set -e
set -x

SCRIPT_DIR="$(dirname "${PWD}/$0")"
export GIT_VERSION=$(git describe --tags --long --always)

function build() {
    # Build program
    cd "${SCRIPT_DIR}/../"
    if [[ -z ${GOARCH} ]]
    then
        export GOARCH=amd64 # OR GOARCH=mipsle
    fi
    CGO_ENABLED=0 go build -ldflags "-s -w -X main.version=${GIT_VERSION}" -a -o openwrt/pkg_dir/usr/sbin/owp-conntrack-event-collector
}

build

# Copy config and init file
cd "${SCRIPT_DIR}"

if [[ -z ${OPKGARCH} ]]
then
    export OPKGARCH="x86_64"
fi

# Make package
sed -i "s/Version: .*/Version: ${GIT_VERSION}/g" pkg_dir/CONTROL/control
sed -i "s/Architecture: .*/Architecture: ${OPKGARCH}/g" pkg_dir/CONTROL/control
./ipkg-build -c -o 0 -g 0 pkg_dir